#include <stdio.h>
#include <switch.h>
#include <malloc.h>
#include <stdlib.h>

#define TRY_LOG(rc, func) do { \
    if (rc) { \
        printf("Error at " func ": %X\n", rc); \
        b_press_exit(1); \
    } \
} while(false)

#define SHOW_MEM_DETAIL(address) do { \
	TRY_LOG(svcQueryMemory(&meminfo, &pageinfo, address), "svcQueryMemory"); \
	printf(#address " - Addr: %lX\tSize: %lX\tType: %X\tPerm: %X\n", meminfo.addr, meminfo.size, meminfo.type, meminfo.perm); \
} while(false)

void b_press_exit(u32 error) {
	puts("Press B to exit.");
	consoleUpdate(NULL);

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B) {
			consoleExit(NULL);
			exit(error);
		}
	}
}

int main() {
	consoleInit(NULL);

	const u64 size = 0x1000;

	u64 space_mapped_rx = (u64)virtmemReserve((size_t)size); // For R-X perm.
    u64 space_mapped_rw = (u64)virtmemReserve((size_t)size); // For R-W perm.
    // We are alligning with pages (size 0x1000), leaving 1 behind and getting one for ours.
	u64 space = (u64)memalign(0x1000, (size_t)size); // After we map CodeMemory, there will be no perms.
	MemoryInfo meminfo;
	u32 pageinfo;

	// Makes CodeMemory.
	TRY_LOG(svcMapProcessCodeMemory(envGetOwnProcessHandle(), space_mapped_rx, space, size), "svcMapProcessCodeMemory");
	SHOW_MEM_DETAIL(space);
	SHOW_MEM_DETAIL(space_mapped_rx);
	TRY_LOG(svcSetProcessMemoryPermission(envGetOwnProcessHandle(), space_mapped_rx, size, Perm_Rx), "svcSetProcessMemoryPermission");
	SHOW_MEM_DETAIL(space_mapped_rx);

	TRY_LOG(svcMapProcessMemory((void *)space_mapped_rw, envGetOwnProcessHandle(), space_mapped_rx, size), "svcMapProcessMemory");
	SHOW_MEM_DETAIL(space_mapped_rw);

	*(u32 *)space_mapped_rw = 0x14000084;
	printf("Value on R-W: %08X\tValue on R-X: %08X\n", *(u32 *)space_mapped_rw, *(u32 *)space_mapped_rx);

	TRY_LOG(svcUnmapProcessMemory((void *)space_mapped_rw, envGetOwnProcessHandle(), space_mapped_rx, size), "svcUnmapProcessMemory");
	SHOW_MEM_DETAIL(space_mapped_rw);
	TRY_LOG(svcUnmapProcessCodeMemory(envGetOwnProcessHandle(), space_mapped_rx, space, size), "svcUnmapProcessMemory");
	SHOW_MEM_DETAIL(space_mapped_rx);

	consoleUpdate(NULL);

	b_press_exit(0);
}
