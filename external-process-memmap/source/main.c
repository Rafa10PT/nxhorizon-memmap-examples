#include <stdio.h>
#include <switch.h>
#include <malloc.h>
#include <stdlib.h>

// In this example, it's the TID of Mario Kart 8 Deluxe.
#define GAME_TID 0x0100152000022000

#define TRY_LOG(rc, func) do { \
    if (rc) { \
        printf("Error at " func ": %X\n", rc); \
        b_press_exit(1); \
    } \
} while(false)

#define SHOW_MEM_DETAIL(address) do { \
	TRY_LOG(svcQueryMemory(&meminfo, &pageinfo, address), "svcQueryMemory"); \
	printf(#address " - Addr: %lX\tSize: %lX\tType: %X\tPerm: %X\n", meminfo.addr, meminfo.size, meminfo.type, meminfo.perm); \
} while(false)

Result pmdmntAtmosphereGetProcessInfo(Handle *outProcess, u64 *outTitleId, NcmStorageId *outStorageId, u64 pid) {
	Service *pmdmnt = pmdmntGetServiceSession();

	Handle outDebugHandle;

	struct {
		u64 titleId;
		NcmStorageId storageId;
	} outData;

	Result rc = serviceDispatchInOut(pmdmnt, 65000, pid, outData, .out_handle_attrs = { SfOutHandleAttr_HipcCopy }, .out_handles = &outDebugHandle);

	if(R_SUCCEEDED(rc)) {
		if(outTitleId != NULL)
			*outTitleId = outData.titleId;

		if(outStorageId != NULL)
			*outStorageId = outData.storageId;

		if(outProcess != NULL)
			*outProcess = outDebugHandle;
	}

	return rc;
}

void b_press_exit(u32 error) {
	puts("Press B to exit.");
	consoleUpdate(NULL);

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B) {
			consoleExit(NULL);
			exit(error);
		}
	}
}

void display_menu(u32 cur_proc, u64 *pid_list, u64 *tid_list) {
	consoleClear();
	printf(
		"Process Select: DPadUp Button or DPadDown Button\nEnter: A Button\nExit: B button\n"
		"Memory indexed number: %04d\n"
		"PID: %lX\n"
		"TID: %016lX\n",
		cur_proc, pid_list[cur_proc], tid_list[cur_proc]
	);
	consoleUpdate(NULL);
}

u32 menu_key_interact(u32 total_proc, u64 *pid_list, u64 *tid_list) {
	u32 cur_proc = total_proc - 2;
	display_menu(cur_proc, pid_list, tid_list);
	while(true) {
		hidScanInput();
		u32 k_down = hidKeysDown(CONTROLLER_P1_AUTO);

		if(k_down & KEY_A)
			return pid_list[cur_proc];

		else if(k_down & KEY_B)
			b_press_exit(0);

		else if(k_down & KEY_DUP) {
			if(cur_proc > 0) --cur_proc;
			display_menu(cur_proc, pid_list, tid_list);
		}

		else if(k_down & KEY_DDOWN) {
			if(cur_proc < total_proc - 2) ++cur_proc;
			display_menu(cur_proc, pid_list, tid_list);
		}
	}
}

void get_process_list_info(int process_limit, u32 *total_proc, u64 *pid_list, u64 *tid_list) {
	TRY_LOG(svcGetProcessList(total_proc, pid_list, process_limit), "svcGetProcessList"); // *num_out, *pid_out, max_pids

	u32 cur_proc = 0;
	for(; cur_proc < *total_proc; ++cur_proc)
		pminfoGetProgramId(&tid_list[cur_proc], pid_list[cur_proc]);

	cur_proc -= 2;
}

int main() {
	consoleInit(NULL);

	pminfoInitialize();
	pmdmntInitialize();

	const u32 proc_lim = 0x50; 
	u64 pid_list[proc_lim];
	u64 tid_list[proc_lim];
	u32 total_proc;
	get_process_list_info(proc_lim, &total_proc, pid_list, tid_list);

	Handle proc_handle;
	u64 pid = menu_key_interact(total_proc, pid_list, tid_list);
	TRY_LOG(pmdmntAtmosphereGetProcessInfo(&proc_handle, NULL, NULL, pid), "pmdmntAtmosphereGetProcessInfo");

	MemoryInfo meminfo;
	u32 pageinfo;
	u64 main_rx_addr = 0;
	u64 main_rx_size = 0;
	u64 addr = 0;
	u8 plausibleReg = 0;
	while(true) {
		svcQueryProcessMemory(&meminfo, &pageinfo, proc_handle, addr);

		if(!(addr + meminfo.size) || plausibleReg > 1)
			break;

		if(meminfo.type == MemType_CodeStatic && meminfo.perm == Perm_Rx) {
			main_rx_addr = meminfo.addr;
			main_rx_size = meminfo.size;
			++plausibleReg;
		}

		addr += meminfo.size;
	}

	u64 procmem_mapped_rw = (u64)virtmemReserve((size_t)main_rx_size); // For R-X perm.
	TRY_LOG(svcMapProcessMemory((void *)procmem_mapped_rw, proc_handle, main_rx_addr, main_rx_size), "svcMapProcessMemory");
	SHOW_MEM_DETAIL(procmem_mapped_rw);
	printf("Value on R-W inside current process: %08X\n", *(u32 *)(procmem_mapped_rw + 8));
	TRY_LOG(svcUnmapProcessMemory((void *)procmem_mapped_rw, proc_handle, main_rx_addr, main_rx_size), "svcUnmapProcessMemory");
	SHOW_MEM_DETAIL(procmem_mapped_rw);

	consoleUpdate(NULL);

	pminfoExit();
	pmdmntExit();

	b_press_exit(0);
}
