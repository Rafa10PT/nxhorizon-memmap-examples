#include <stdio.h>
#include <switch.h>
#include <malloc.h>
#include <stdlib.h>

#define TRY_LOG(rc, func) do { \
    if (rc) { \
        printf("Error at " func ": %X\n", rc); \
        b_press_exit(1); \
    } \
} while(false)

#define SHOW_MEM_DETAIL(address) do { \
	TRY_LOG(svcQueryMemory(&info, &pageinfo, address), "svcQueryMemory"); \
	printf(#address " - Addr: %lX\tSize: %lX\tType: %X\tPerm: %X\n", info.addr, info.size, info.type, info.perm); \
} while(false)

void b_press_exit(u32 error) {
	puts("Press B to exit.");
	consoleUpdate(NULL);

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B) {
			consoleExit(NULL);
			exit(error);
		}
	}
}

int main() {
	consoleInit(NULL);

	const u64 size = 0x1000;

	u64 space_mapped_rw = (u64)virtmemReserveStack(size);
	u64 space = (u64)memalign(0x1000, size);

	*(u32 *)space = 0xAAAAAAAA;
	printf("space - Val: %X\n", *(u32 *)space);
	// The syscall svcMapMemory is used to move a region.
	// It can only map on stack and the source region will loose its perms.
	TRY_LOG(svcMapMemory((void *)space_mapped_rw, (void *)space, size), "svcMapMemory");
	MemoryInfo info;
	u32 pageinfo;
	SHOW_MEM_DETAIL(space_mapped_rw);
	SHOW_MEM_DETAIL(space);

	printf("space_mapped_rw - Val: %X\n", *(u32 *)space_mapped_rw);
	// To unmap the new region and give the perms back to the source region, svcUnmapMemory is used.
	TRY_LOG(svcUnmapMemory((void *)space_mapped_rw, (void *)space, size), "svcUnmapMemory");
	SHOW_MEM_DETAIL(space_mapped_rw);
	SHOW_MEM_DETAIL(space);

	consoleUpdate(NULL);

	b_press_exit(0);
}
