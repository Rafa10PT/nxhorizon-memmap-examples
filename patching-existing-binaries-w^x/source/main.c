#include <stdio.h>
#include <switch.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#define TRY_LOG(rc, func) do { \
    if (rc) { \
        printf("Error at " func ": %X\n", rc); \
        b_press_exit(1); \
    } \
} while(false)

#define SHOW_MEM_DETAIL(address) do { \
	TRY_LOG(svcQueryMemory(&meminfo, &pageinfo, (u64)address), "svcQueryMemory"); \
	printf(#address " - Addr: %lX\tSize: %lX\tType: %X\tPerm: %X\n", meminfo.addr, meminfo.size, meminfo.type, meminfo.perm); \
} while(false)

#define TEST_INCREMENT_FUNC() do { \
	puts("Executing test function..."); \
	test(&v); \
	printf("Val: %X\n", v); \
} while(false)

void b_press_exit(u32 error) {
	puts("Press B to exit.");
	consoleUpdate(NULL);

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B) {
			consoleExit(NULL);
			exit(error);
		}
	}
}

void patch_loadNro() {
	MemoryInfo meminfo;
	u32 pageinfo;
	u64 addr = 0;

	// Finding main NSO.
    do {
    	TRY_LOG(svcQueryMemory(&meminfo, &pageinfo, addr), "svcQueryMemory");
    	if(meminfo.type == 3 && meminfo.perm == 5)
    		break;
    	addr = meminfo.addr + meminfo.size;
    } while(addr != 0);

	const u32 size = 0x1000;
	u64 nso_mapped_rw = (u64)virtmemReserve(size);
	TRY_LOG(svcMapProcessMemory((void *)nso_mapped_rw, envGetOwnProcessHandle(), addr, size), "svcMapProcessMemory");
	SHOW_MEM_DETAIL(addr);
	SHOW_MEM_DETAIL(nso_mapped_rw);

	// We patch loadNro, so that open is called and a file is made.
	// We jump to open with the path and O_CREAT (0x200) as args.
	static const struct {
		u32 asm_patch[5];
		char filename[31];
	} patch = { {
			0x90000000, 0x91025000, 0x52804001, 0xB9400A77,
			0x1400252C
		},
		"sdmc:/test-w^x-result-file.txt"
	};

	// 0x9C0 is an offset inside loadNro function where we are hooking.
	*(u32 *)(nso_mapped_rw + 0x9C0) = 0x97FFFDB0;
	armICacheInvalidate((void *)(addr + 0x9C0), sizeof(u32));
	armDCacheFlush((void *)(nso_mapped_rw + 0x9C0), sizeof(u32));

	// Jump to 0x80 offset.
	memcpy((void *)(nso_mapped_rw + 0x80), (void *)&patch, 31 * sizeof(char) + 5 * sizeof(u32));
	armICacheInvalidate((void *)(addr + 0x80), 31 * sizeof(char) + 5 * sizeof(u32));
	armDCacheFlush((void *)(nso_mapped_rw + 0x80), 31 * sizeof(char) + 5 * sizeof(u32));

	TRY_LOG(svcUnmapProcessMemory((void *)nso_mapped_rw, envGetOwnProcessHandle(), addr, size), "svcUnmapProcessMemory");

	consoleUpdate(NULL);

	b_press_exit(0);
}

void a_patch_nso_load_b_press_exit() {
	puts(
		"Test to patch NRO load function to make a test-w^x-result-file.txt file on SD root.\n"
		"There will be a call to open with the path and O_CREAT as arg each time loadNro is called.\n"
		"Press A to patch main NSO (tested on nx-hbloader v2.3.0 only) and B to exit.\n"
	);

	consoleUpdate(NULL);

	while(true) {
		hidScanInput();
		u32 kDown = hidKeysDown(CONTROLLER_P1_AUTO);
		if(kDown & KEY_B) {
			consoleExit(NULL);
			exit(0);
		}
		if(kDown & KEY_A) {
			patch_loadNro();
		}
	}
}

__attribute__((noinline)) void test(u32 *v) {
	// puts("Hello World!");
	++*v;
}

int main() {
	consoleInit(NULL);

	// Address must be page alingned.
	u64 addr = (u64)test &~ 0xFFF;
	u64 size = (((u64)test & 0xFFF) + 4 + 0xFFF) & ~0xFFF; 

	u64 test_mapped_rw = (u64)virtmemReserve((size_t)size);

	TRY_LOG(svcMapProcessMemory((void *)test_mapped_rw, envGetOwnProcessHandle(), addr, size), "svcMapProcessMemory");
	MemoryInfo meminfo;
	u32 pageinfo;
	SHOW_MEM_DETAIL(test_mapped_rw);
	printf("test - Addr: %lX\tBase Map Addr: %lX\n", (u64)test, addr);
	SHOW_MEM_DETAIL(test);

	u32 v = 1;
	printf("Val: %X\n", v);
	TEST_INCREMENT_FUNC();

	// Stubbing the function.
	printf("Old: %X\n", *(u32 *)(test_mapped_rw	+ ((u64)test - addr)));
	*(u32 *)(test_mapped_rw	+ ((u64)test - addr)) = 0xD65F03C0; // RET
	printf("New: %X\n", *(u32 *)(test_mapped_rw	+ ((u64)test - addr)));

	// Invalidating icache, so that the CPU can process the RET instruction.
	armICacheInvalidate((void *)test, sizeof(u32));
	// I think CPU can still end up with the original instruction if dcache is not flushed.
	// This must be done before unmapping the memory or it crashes.
	armDCacheFlush((void *)(test_mapped_rw + ((u64)test - addr)), sizeof(u32));

	TEST_INCREMENT_FUNC();
	printf("The variable shouldn't have been incremented to 3.\n");

	TRY_LOG(svcUnmapProcessMemory((void *)test_mapped_rw, envGetOwnProcessHandle(), addr, size), "svcUnmapProcessMemory");

	consoleUpdate(NULL);

	a_patch_nso_load_b_press_exit();
}
